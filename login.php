<?php
session_start();
//include 'common.php';
require_once 'config.php';
require_once 'function.php';

//include 'function.php';
?>
<?php
 
 if (!empty($_POST["mob_flag"])) {
   $type = "mob";
   $typevalue = "mobile";
} else if (!empty($_POST["dth_flag"])) {
    $type = "dth";
     $typevalue = "dth";
} else if (!empty($_POST["data_flag"])) {
    $type = "data";
     $typevalue = "data";
} else if (!empty($_POST["post_flag"])) {
    $type = "post";
     $typevalue = "postpaid";
}
 
 if(isset($_POST) && !empty($type)){
   $flag = $_POST[$type."_flag"];
   $id = $_POST[$type."_id"];
   $min = $_POST[$type."_min"];
   $max = $_POST[$type."_max"];
   $amount = $_POST[$type."_amount"];
   $number = $_POST[$type."_number"];
   $provider = $_POST[$type."_provider"];
   $circle = $_POST[$type."_circle"];
   $charge = $_POST[$type."_charge"];
    $product = $_POST[$type."_product"];

 }
if(isset($_GET["logout"])){
    
     $service_url = CDEV_URL."/index.php/api_new/action/api/true/actiontype/signout/";
     $curl_response = getCurlRespose($service_url);
        $status = $curl_response->status;
        if ($status == "success") {
            unset($_SESSION["flag"]);
            session_destroy();
            
        }
        
        
}
?>

<?php
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
   
   $method = $_POST["method"];
   $mobileno = $_POST["mobileno"];
   $service_url = PANEL_URL."/apis/receiveWeb/mindsarray/mindsarray/json?method=$method&mobile=$mobileno";
   $curl_response = getCurlAjaxRespose($service_url);
    $curl_response = trim($curl_response);
    $curl_response = trim($curl_response, ")");
    $curl_response = trim($curl_response, "(");
    $curl_response = trim($curl_response, ";");
    $curl_response = substr_replace($curl_response, "", -1);
    $curl_response = json_decode($curl_response);
    $curl_response = objectToArray($curl_response);
    $data = array();
    foreach ($curl_response as $datakey => $datavalue) {
    foreach ($datavalue as $key => $value) {
        foreach ($value as $k => $v) {
            $data[$v["number"]] = $v;
        }
    }
   }
    $data = json_encode($data);
    echo $data;
    die;
   
}
?>
<html lang="en">
    <head>
        <title>Signup || PAY1</title>
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
        <link rel="shortcut icon" type="image/png" href="/images/pay1.png"/>
         <link rel="stylesheet" type="text/css" href="css/global.css">
</head>
<body>           
<?php require_once 'left.php';
       include 'analyticstracking.php';
 ?>    
<div class="shiftbox">
    <div class="hutpart" style="height:100%;width:100%; display:block; position:absolute;">
        <div class="container">
            <div class="row mT20">
                <div class="col-md-6 col-md-offset-3">
                    <div class="signlogBOX">
                        <span style="color:red;" id="loginerr"><?php echo $loginErr; ?></span>
<!--						<span class="btn-info">Login</span>-->
                        <!--						<form class="form mT20" role="form">-->
                        <form method="post" class="form mT20" id ="target" action="index.php?<?php echo $typevalue; ?>">
                            <input type="hidden" name="rec_type" value="<?php echo $flag; ?>">
                            <input type="hidden" name="type" value="<?php echo $type; ?>">
                            <input type="hidden" name="rec_id" value="<?php echo $id; ?>">
                            <input type="hidden" name="rec_min" value="<?php echo $min; ?>">
                            <input type="hidden" name="rec_max" value="<?php echo $max; ?>">
                            <input type="hidden" name="rec_amount" value="<?php echo $amount; ?>">
                            <input type="hidden" name="rec_number" value="<?php echo $number; ?>">
                            <input type="hidden" name="rec_provider" value="<?php echo $provider; ?>">
                            <input type="hidden" name="rec_circle" value="<?php echo $circle; ?>">
                            <input type="hidden" name="rec_charge" value="<?php echo $charge; ?>">
                            <input type="hidden" name="rec_product" value="<?php echo $product; ?>">
                            <div class="form-group">
                                <label class="" for="">Mobile Number</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="loginsign-mobile"></span>
                                    </div>
                                    <input class="form-control"  onkeypress="return isNumberKey(event)" id ="mob-number" name="mob-number" type="text" autocomplete="off" maxlength="10">
                                </div>
                                <span style="color:red;"> <?php echo $mobErr; ?></span>
                                <br>
                            </div>
                            <div class="form-group">
                                <label class="" for="exampleInputEmail2">Pin</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="loginsign-lock"></span>
                                    </div>
                                    <input class="form-control" autocomplete="off" name ="password" id ="password" type="password">
                                </div>
                                <span style="color:red;"><?php echo $passErr; ?></span>
                                <br>
                                
                            </div>
                            <div class="form-group">
                                <span class="termncond">By signing up you agree to our <a href="#">Terms &amp; Conditions.</a></span>
                                <button type="button" id="login" class="btn btn-primary btn-lg btn-block">Login</button>
                                
                                <div class="text-right" style="margin-top:15px"><a href="forgot_password.php">Forgot pin</a></div>
                                <p class="logOR">Or</p>
                                <a href="sign-up.php" class="btn btn-default btn-lg btn-block">Sign up</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- //shiftbox -->
<!--</form>-->
</div>
<script src="js/jquery.min.2.1.1.js"></script>

<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>
    $('#password').keydown(function(event){
        if (event.keyCode == 13) {
        var mobileNo = $("#mob-number").val();
        var password = $("#password").val();
        if(mobileNo=='')
        {
            alert("Please Enter mobile no.");
            $("#mob-number").focus();
            return false;
        }
        else if(password=='')
        {
            alert("Password can not be blank");
            $("#password").focus();
            return false;
        }
        else
        {
           
            var url = "<?Php echo CDEV_URL; ?>/index.php/api_new/action/actiontype/signin/api/true/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{username:mobileNo,
                  password:password,
                  uuid:"",
                  latitude:"",
                  logitude:"",
                  device_type:"",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
               if(data.status=="success")
               {
                   setdatavalue(data);
               }
               else
               {
                   $("#loginerr").html(data.description);
               }
               
            },
            error: function (xhr,error) {
             },  
         
            });
        }
        }
});

    $('#login').click(function(event){
        var mobileNo = $("#mob-number").val();
        var password = $("#password").val();
        if(mobileNo=='')
        {
            alert("Please Enter mobile no.");
            $("#mob-number").focus();
            return false;
        }
        else if(password=='')
        {
            alert("Password can not be blank");
            $("#password").focus();
            return false;
        }
        else
        {
           
            var url = "<?Php echo CDEV_URL; ?>/index.php/api_new/action/actiontype/signin/api/true/?";
            $.ajax({
            url: url,
            type:"GET",
            data:{username:mobileNo,
                  password:password,
                  uuid:"",
                  latitude:"",
                  logitude:"",
                  device_type:"",
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
           // jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
              if(data.status=="success")
               {
                   setdatavalue(data);
               }
               else
               {
                   $("#loginerr").html(data.description);
               }
               
            },
            error: function (xhr,error) {
             },  
         
            });
        }
});



function setdatavalue(data)
{
    var mobileNo = data.description["mobile"];
    var name = data.description["name"];
    var email = data.description["email"];
    var dob = data.description["date_of_birth"];
    var longitude =  data.description["longitude"]; 
    var latitude =  data.description["latitude"]; 
    var walletbal = data.description["wallet_balance"];
    var device_type = data.description["device_type"];
    var gender = data.description["gender"];
    var password =  $("#password").val();

       $.ajax({
            url: "info.php",
            type:"POST",
            data:{
                 name:name,
                 email:email,
                 dob:dob,
                 username:mobileNo,
                 device_type:device_type,
                 gender:gender,
                 walletbal : walletbal,
                 password:password
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
                
              if(data.status=="success")
              {
                   $("#target").submit();
              }
              
            },
            error: function (error) {
              

          }
            });
}

function isNumberKey(evt){
 var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true
     }

</script>


<script>
    
$(document).ready(function(){
var operatordata = JSON.parse(<?php echo json_encode($data); ?>); 
var storagedate = localStorage.getItem('datetime');
var date = <?php echo date("Ymd"); ?>;
 var methodtype = "getMobileDetails";
 var mobileno = "all";
if(storagedate!=date){
      $.ajax({
            url: "login.php",
            type:"POST",
            data:{
                 method : methodtype,
                 mobileno: mobileno
                 },
            dataType:"json",
            success:function(data){
              $.each(data,function(operatordatakey,operatordataval){
              localStorage.setItem(operatordatakey, JSON.stringify(operatordataval));
              localStorage.setItem("datetime",date);
              });
  
            },
            error: function (error) {
            }
            });

}
});

    </script>



</body>

</html>