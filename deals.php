<?php
session_start();

if ($_SESSION["flag"] != "true") {

    header("location:login.php?logout=logout");
}
include 'config.php';
include 'common.php';
//include 'function.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Deals || PAY1</title>
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />


<!--        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <link rel="stylesheet" type="text/css" href="css/global.css">
        <![endif]-->
        	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
        <link rel="shortcut icon" type="image/png" href="/images/pay1.png"/>
         <link rel="stylesheet" type="text/css" href="css/global.css">
    </head>
    <body>
        <div>
          
<?php include 'left.php';  ?>

           
            <!-- //close navigation -->
            <div class="shiftbox">
                <div class="hutpart">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
<?php include 'header.php';
//sinclude 'analyticstracking.php'; 
?>
                            </div>
                        </div>
                        <div class="row mT20">
                            <div class="col-sm-3">
                                <div class="menuMOBILESET">
                                    <ul id="myTab" class="nav nav-tabs1 dealslist" role="tablist">
                                        <li class="active">
                                            <a data-toggle="tab" role="tab" id="my_gift" href="#tab1">My Gifts</a>
                                        </li>
                                  
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <div id="ListTabContent" class="tab-content clearfix">
                                    <div class="dealsbread">
                                        <ul>
                                            <li></li>
                                            <li><a href="#">Free Gifts</a></li>
                                            <!--<li>&raquo;</li>-->
                                            <!--<li><span></span></li>-->
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div id="tab1" class="tab-pane fade active in">
                                        <div class="dealfullbox" id="mydeal">


                                        </div>
                                    </div>
                                    
                            </div>

                        </div>
                    </div>
                </div>

                <!-- footer box -->
                <?php include 'footer.php'; ?>

            </div><!-- //shiftbox -->
        </div>
        <script src="js/slides.min.jquery.js"></script>
        <script>
         
         </script>
        <script type="text/javascript">
            $(document).ready(function() {
            var loc = location.search;
            if(loc!=''){
            $(".current").removeClass("current");
            var type = loc.substr(1);
            $("."+type).addClass('current');
            localStorage.setItem("rectype",type);
            }
            if(loc=="?my-gifts"){
                $("#tab1").show();
                $("#my_gift").show();
            } else {
                
                $("#tab2").show();
                $("#free_gift").show();
                //getalldeals();
             }
                var serviceurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/get_my_deal/?";
                var html = '';
                var i = 1;
                $.ajax({
                    url: serviceurl,
                    type: "GET",
                    data: {
                        freebie: "true",
                        res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(dealsdata) {
                        if (dealsdata.status == "success")
                        {
                            $.each(dealsdata.description, function(dealkey, dealvalue) {
                                html += "<div class='dealboxx'>";
                                html += "<div class='media ddll'><dl class='dl-horizontal'>";
                                html += "<dt><img class='media-object' src='" + dealvalue.img_url + "' alt=''></dt>";
                                html += "<dd><h4 class='media-heading' data-toggle='collapse' data-parent='#accordion-" + i + "' data-target='#collapse-" + i + "'>" + dealvalue.deal_name + "</h4>";
                                html += "<div class='col-md-7 leftrightpad mT10 statustext'><p><strong>Coupon status : </strong><span>" + dealvalue.coupon_status + "</span></p><p><strong>Expires : </strong><span>" + dealvalue.expiry + "</span></p></div>";
                                html += "<div class='col-md-5 leftrightpad mT10'><div class='dealslinksLR'>";
                                html += "</div>";
                                html += "</div></dd></dl></div>";
                                html += "<div class='panel-group' id='accordion-" + i + "'>";
                                html += " <div id='collapse-" + i + "' class='panel-collapse collapse'><div class='col-md-12 leftrightpad'>";
                                html += "<h4 class='dealshead'>" + dealvalue.deal_name + "</h4><small class='dealtime'>" + dealvalue.expiry + "</small>";
                                html += "<table class='borderBOTTOM'><tr><th>Deal Code</th><th>&nbsp;</th><th>Quantity Purchased</th></tr><tr><td><p>" + dealvalue.code + "<p></td><td><p>Rs.&nbsp;" + dealvalue.amount + "</p></td><td><p>" + dealvalue.quantity + "</p></td></tr></table>";
                                html += "<table class='borderBOTTOM'><tr><th>Mode of Payment</th><th>Tranaction ID</th></tr><tr><td><p>" + dealvalue.transaction_mode + "</p></td><td><p>" + dealvalue.transaction_id + "</p></td></tr></table></div>";
                                html + "</div></div>"
                                html += "</div>";
                                //console.log(html);
                                $("#mydeal").append(html);
                                html = '';
                                i++;

                            });

                        }
                        else if (dealsdata.errCode == "201") {
                            window.location = "login.php?logout=";
                        }

                    },
                    error: function(error) {
                    }
                });

            });
  
        </script>
    </body>
</html>

          <!-- <a href='#'><span class='dealslinks-qr'></span></a><a href='deals.php'><span class='dealslinks-phone'></span></a><a href='#'><span class='dealslinks-msg'></span></a>!--->                                                            