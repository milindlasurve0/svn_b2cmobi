<?php
session_start();

if ($_SESSION["flag"] != "true") {

    header("location:login.php?logout=logout");
}
include 'config.php';
include 'common.php';
//include 'function.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Deals || PAY1</title>
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />


        <!--        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
                <link rel="stylesheet" type="text/css" href="css/style.css">
                <link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <link rel="stylesheet" type="text/css" href="css/global.css">
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
        <link rel="shortcut icon" type="image/png" href="/images/pay1.png"/>
        <link rel="stylesheet" type="text/css" href="css/global.css">
    </head>
    <body>
        <div>
            
            <?php include 'left.php'; ?>


            <!-- //close navigation -->
            <div class="shiftbox">
                <div class="hutpart">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                include 'header.php';
//sinclude 'analyticstracking.php'; 
                                ?>
                            </div>
                        </div>
                        <div class="row mT20">
                            <div class="col-sm-3">
                                <div class="menuMOBILESET">
                                    <ul id="myTab" class="nav nav-tabs1 dealslist" role="tablist">

                                        <li class="active">
                                            <a data-toggle="tab" role="tab"  onclick="getalldeals('0')"  href="#tab1">Available Free Gifts</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <div id="ListTabContent" class="tab-content clearfix">

                                    <div id="tab1" class="">
                                        <form method ='post' id='deal_form' action='wallet.php'>
                                            <input type='hidden' name='lat' id='lat' value=''>
                                            <input type='hidden' name='shop_id' id='shop_id' value=''>
                                            <input type='hidden' id='long' name='long' value=''>
                                        </form>
                                         <div id="container">
                                             <div id="slides2">
                                         
                                <div class="slides_container2">
                                    
                                    
                                </div>
                                             <a href="javascript:void(0);" class="prev"><img onclick="previousvalue()" src="images/arrow-prev.png" width="24" height="43" alt="Arrow Prev"></a>
				                <a href="javascript:void(0);" class="next"><img  onclick="nextvalue()"src="images/arrow-next.png" width="24" height="43" alt="Arrow Next"></a>
                                
                                    </div>
                                             </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- footer box -->
<?php include 'footer.php'; ?>

                </div><!-- //shiftbox -->
            </div>
            <script src="js/slides.min.jquery.js"></script>
            <script>
                function slidecontainerimage(j){      
		        $(".slides_container2").html('');
                  $(".slides_container2").append(associativearray[j]);
		          if(associativearray.length-3==k){
                       //i++;
                       getalldeals();
                    }
                    
            	window.location.hash = '#' + k;
				    }
            </script>
            <script type="text/javascript">
        associativearray = new Array();
         var k = 1;      
         var m = 1;
         var n = 0;
                
       function getalldeals()
      {
      //$(".slides_container").html('');
      var html = "";    
      var newElement = {};
      var offerId = '';
      var serviceurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/GetupdatedDeal/?";
      var latitude = localStorage.getItem("latitude");
      var longitude = localStorage.getItem("longitude");
    
      var amount = localStorage.getItem('amount');
      var amountArray = new Array();
      var freebieMinValue = new Array();
      var freebieMaxValue = new Array();
      var mobile = "9967331335";
      var updatedTime = "";
      //var nextparam = i;
      
      
      
         $.ajax({
            url: serviceurl,
            type:"GET",
             data:{
                 user_mobile:mobile,
                 longitude: longitude,
                 latitude : latitude,
                 updatedTime: updatedTime,
                 next:n,
                 res_format : "jsonp"
                 },
          
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(dealsdata){
              if(dealsdata.status=="success")
              {  
                  offerId = dealsdata.description.offer_details.split(",");
                    
                 $.each(dealsdata.description.Alldeals,function(dealkey,dealvalue){
                     newElement[dealvalue.of_id] = dealvalue;
                  });
                  var expiredOffer = dealsdata.description.ExpiredOffers.split(",");
                  var claimedGifts = dealsdata.description.claimedGifts.split(",");
                    $.each(offerId,function(offerkey,offerval){
                       // console.log(offerval);
                           //if(expiredOffer.indexOf(offerval)>-1 || claimedGifts.indexOf(offerval)>-1 ){
                           var dealdetails = newElement[offerval];
                           var imageurl = dealdetails.i_url;
                           var deal_id  = "\'" + dealdetails.id + "\'";
                            html+="<div class='slide'><b><span class=''>"+dealdetails.name+"</span></b><b></b><div class=''><img class='' height='300px;' width='300px;' src='"+imageurl+"'>";
                            html+="<ul class='hutLIST clearfix'>";
                            html+="<li style='color:red;'><small><span style='align:center;'></small></span></li>";
                            html+="</ul></div>";
                            html+="<div class=''>";
                            html+="<h3>"+dealdetails.of_name+"</h3><h2>"+dealdetails.locs[0].addr+"</h2>"
                            html+='<a href="javascript:getdeallocation('+dealdetails.locs[0].lat+','+dealdetails.locs[0].lng+','+deal_id+')"><img  src="images/map-icon.png"></a>'
                            html+="</div>";
                            html+="<div>&nbsp;</div><div style=\"align:center\"><a href='index.php'><button class='common-btn-submit'>Recharge Now</button></div></a>"
                            html+="<div><h2 style=\"color:red;\">Valid for min Rs."+dealdetails.min+"+ Recharge</h2></div>"
                            html+="</div>"
                            associativearray[m] = html;
                            m++;
                            html = '';
                        });
                     n++;
                     slidecontainerimage(k);
                 
             
              }},
            error: function (error) {
            }
            });
    
}

function previousvalue(){
    if(k==1){

    } else {
              k--;
              slidecontainerimage(k);
        }
}

function nextvalue(){
    if(k>=1){
        k++;
        slidecontainerimage(k);
       } 
  }
               
            </script>
    </body>
</html>

          <!-- <a href='#'><span class='dealslinks-qr'></span></a><a href='deals.php'><span class='dealslinks-phone'></span></a><a href='#'><span class='dealslinks-msg'></span></a>!--->                                                            