
$("[data-idd='#mobnav']").click(function() {
    var href = $(this).attr("data-idd");
    $(href).toggle();
    $(".opacitybg").slideToggle(500);

});



$(document).ready(function() {
    $(".show-1").show();
    $(".show-2").show();
    $(".show-3").show();
    $(".show-4").show();
    $(".show-5").show();
    $(".show-6").show();

    $(".show-1").click(function() {
        $(".alllinkshow-1").slideToggle();
        $(".show-1").toggleClass("collapsinglink");
        return false;
    });

    $(".show-2").click(function() {
        $(".alllinkshow-2").slideToggle();
        $(".show-2").toggleClass("collapsinglink");
        return false;
    });

    $(".show-3").click(function() {
        $(".alllinkshow-3").slideToggle();
        $(".show-3").toggleClass("collapsinglink");
        return false;
    });

    $(".show-4").click(function() {
        $(".alllinkshow-4").slideToggle();
        $(".show-4").toggleClass("collapsinglink");
        return false;
    });

    $(".show-5").click(function() {
        $(".alllinkshow-5").slideToggle();
        $(".show-5").toggleClass("collapsinglink");
        return false;
    });

    $(".show-6").click(function() {
        $(".alllinkshow-6").slideToggle();
        $(".show-6").toggleClass("collapsinglink");
        return false;
    });

});



function acknowledgeopen() {
    var aa = document.getElementById('acknowpopup');
    aa.style.display = "block";
}

function acknowledgeclose() {
    var bb = document.getElementById('acknowpopup');
    bb.style.display = "none";
}


function show(id) {
    var cs = document.getElementById(id);
    cs.style.display = "block";
}
function hide(idd) {

    var ch = document.getElementById(idd);

    ch.style.display = "none";
}


function getfreebie(modal)
{
    $("#" + modal).modal('hide');
    $('#congratulations').modal('toggle');
    showfreebie();
    slideimage();
    if(localStorage.getItem("latitude")!='' && localStorage.getItem("longitude")!=''){
    getLocation();
}
   
}

function getdeallocation(lat, lon, deal_id)
{
    $("#lat").val(lat);
    $("#long").val(lon);
    $("#shop_id").val(deal_id);
    $("#deal_form").submit();

}

function setRechargeOperatorValue(type, operatorid, value)
{

    $("#operator_type").val(type);
    $("#operator_id").val(operatorid);
    $("#operator_value").val(value);
    document.getElementById('footer_form').action = "index.php?" + value
    document.getElementById('footer_form').submit();

}
function getLocation() {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        
    } else { 
          alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {

       var latitude = position.coords.latitude;
       var longitude = position.coords.longitude;
       
       if(latitude!='' && longitude!=''){
        localStorage.setItem("latitude",latitude);
        localStorage.setItem("longitude",longitude);
        localStorage.setItem("showmodal","true");
        //$('#congratulations').modal('hide');
        
        $.ajax({
            url: "info.php",
            type:"POST",
            data:{
                 latitude : latitude,
                 longitude: longitude
                 },
            dataType:"json",
            success:function(data){
                slideimage();
                showfreebie();
            },
            beforeSend: function(){
               $('.loader').show();
               },
            complete: function(){
             $('.loader').hide();
             },
            error: function (error) {
               // console.log(error);
            }
            });
        }
             
            

}


